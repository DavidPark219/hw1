package data;

import com.fasterxml.jackson.core.*;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.IOException;
import java.nio.file.Path;

/**
 * @author Ritwik Banerjee
 * @author David Park
 */
public class GameDataFile implements AppFileComponent {

    public static final String TARGET_WORD  = "TARGET_WORD";
    public static final String GOOD_GUESSES = "GOOD_GUESSES";
    public static final String BAD_GUESSES  = "BAD_GUESSES";
    public static final String REMAINING_GUESSES = "REMAINING_GUESSES";

    public void saveData(AppDataComponent data, Path to) throws IOException {
        GameData gameData = (GameData) data;

        JsonFactory jsonFactory = new JsonFactory();
        JsonGenerator jsonGenerator = jsonFactory.createGenerator(to.toFile(), JsonEncoding.UTF8);
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField(TARGET_WORD, gameData.getTargetWord());
        jsonGenerator.writeStringField(GOOD_GUESSES, gameData.getGoodGuesses().toString());
        jsonGenerator.writeStringField(BAD_GUESSES, gameData.getBadGuesses().toString());
        jsonGenerator.writeNumberField(REMAINING_GUESSES, gameData.getRemainingGuesses());
        jsonGenerator.writeEndObject();
        jsonGenerator.close();
    }

    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {
    }

    public void loadData(GameData gameData, Path from) throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        JsonParser jsonParser = jsonFactory.createParser(from.toFile());

        while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = jsonParser.getCurrentName();
            // checks each field for the one we're looking for
            if (TARGET_WORD.equals(fieldName)) {
                jsonParser.nextToken();
                gameData.setTargetWord(jsonParser.getText());
            }
            if (GOOD_GUESSES.equals(fieldName)) {
                jsonParser.nextToken();
                String array = jsonParser.getText();
                array = array.replace("[", "").replace("]","").replace(" ","").replace(",","");
                for (int i = 0; i < array.length(); i++) {
                    gameData.addGoodGuess(array.charAt(i));
                }
            }
            if (BAD_GUESSES.equals(fieldName)) {
                jsonParser.nextToken();
                String array = jsonParser.getText();
                array = array.replace("[", "").replace("]","").replace(" ","").replace(",","");
                for (int i = 0; i < array.length(); i++) {
                    gameData.addBadGuess(array.charAt(i));
                }
            }
            if (REMAINING_GUESSES.equals(fieldName)) {
                jsonParser.nextToken();
                gameData.setRemainingGuesses(jsonParser.getIntValue());
            }
        }
        jsonParser.close();
    }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }
}
