package controller;

import apptemplate.AppTemplate;
import data.GameData;
import data.GameDataFile;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import static settings.AppPropertyType.*;

/**
 * @author Ritwik Banerjee
 * @author David Park
 */
public class HangmanController implements FileController {

    private AppTemplate appTemplate; // shared reference to the application
    private static GameData gameData;    // shared reference to the game being played, loaded or saved
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private static boolean     gameover = true;    // whether or not the current game is already over
    private static boolean     savable;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void start() {
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        workspace.reinitialize();
        gameData = new GameData(appTemplate);
        gameover = false;
        success = false;
        savable = true;
        discovered = 0;
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);

        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(guessedLetters);
        gameButton.setDisable(true);
        play();
    }

    private void end() {
        System.out.println(success ? "You win!" : "Ah, close but not quite there. The word was \"" + gameData.getTargetWord() + "\".");
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameover = true;
        gameButton.setDisable(false);
        savable = false; // cannot save a game that is already over
        appTemplate.getGUI().updateWorkspaceToolbar(savable);

    }

    private void initWordGraphics(HBox guessedLetters) {
        char[] targetword = gameData.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
        }
        guessedLetters.getChildren().addAll(progress);
    }

    public void play() {
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                if (gameData.getRemainingGuesses() <= 0 || success)
                    stop();
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    if (Character.isLetter(event.getCharacter().charAt(0))) {
                        char guess = event.getCharacter().charAt(0);
                        if (!alreadyGuessed(guess)) {
                            savable = true;  //makes sure any changes trigger the savable state
                            appTemplate.getGUI().updateWorkspaceToolbar(savable);
                            boolean goodguess = false;
                            for (int i = 0; i < progress.length; i++) {
                                if (gameData.getTargetWord().charAt(i) == guess) {
                                    progress[i].setVisible(true);
                                    gameData.addGoodGuess(guess);
                                    goodguess = true;
                                    discovered++;
                                    success = (discovered == progress.length);
                                }
                            }
                            if (!goodguess) {
                                gameData.addBadGuess(guess);
                            }
                            remains.setText(Integer.toString(gameData.getRemainingGuesses()));
                        }
                    }
                });
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private boolean alreadyGuessed(char c) {
        return gameData.getGoodGuesses().contains(c) || gameData.getBadGuesses().contains(c);
    }
    
    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (savable) {
            try {
                YesNoCancelDialogSingleton saveDialog = YesNoCancelDialogSingleton.getSingleton();
                saveDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE), propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));
                if (saveDialog.getSelection().equalsIgnoreCase("YES"))
                    makenew = promptToSave();
                if (saveDialog.getSelection().equalsIgnoreCase("CANCEL"))
                    makenew = false;
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            ensureActivatedWorkspace();                            // ensure workspace is activated

            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

        if (gameover) {
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        FileChooser save = new FileChooser();
        File savesDir = new File("./Hangman/saves");
        boolean saves = savesDir.mkdir();
        save.setInitialDirectory(savesDir);
        save.setInitialFileName("GAME1");
        save.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JSON", "*.json"));
        File file = save.showSaveDialog(appTemplate.getGUI().getWindow());
        if (file != null) {
            Path path = file.toPath();
            GameDataFile gameDataFile = new GameDataFile();
            gameDataFile.saveData(gameData, path);
            savable = false;
        }
    }

    @Override
    public void handleLoadRequest() throws IOException {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   load         = true;

        if (savable) {
            try {
                YesNoCancelDialogSingleton saveDialog = YesNoCancelDialogSingleton.getSingleton();
                saveDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE), propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));
                if (saveDialog.getSelection().equalsIgnoreCase("YES"))
                    load = promptToSave();
                if (saveDialog.getSelection().equalsIgnoreCase("CANCEL"))
                    load = false;
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        }
        if (load) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            ensureActivatedWorkspace();                            // ensure workspace is activated

            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();

            gameData = new GameData(appTemplate);
            gameover = false;
            success = false;
            savable = false;
            discovered = 0;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
            HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);

            FileChooser loadFile = new FileChooser();
            File loadDir = new File("./Hangman/saves");
            boolean saves = loadDir.mkdir();
            loadFile.setInitialDirectory(loadDir);
            loadFile.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JSON", "*.json"));
            File file = loadFile.showOpenDialog(appTemplate.getGUI().getWindow());
            if (file != null) {
                Path path = file.toPath();
                GameDataFile gameDataFile = new GameDataFile();
                gameDataFile.loadData(gameData, path);
                savable = false; //newly loaded filed can't be saved
            }

            remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
            remains.setText(Integer.toString(gameData.getRemainingGuesses()));
            remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
            initWordGraphics(guessedLetters);
            String check = gameData.getGoodGuesses().toString().replace("[", "").replace("]","").replace(" ","").replace(",","");
            for (int i = 0; i < progress.length; i++) {
                if (check.contains(progress[i].getText())) {
                    progress[i].setVisible(true);
                    discovered++;
                }
            }
            gameButton.setDisable(true);
            play();
        }
        if (gameover) {
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }
    }

    @Override
    public void handleExitRequest() {
        PropertyManager propertyManager  = PropertyManager.getManager();
        try {
            boolean exit = true;
            if (savable) {
                YesNoCancelDialogSingleton saveDialog = YesNoCancelDialogSingleton.getSingleton();
                saveDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE), propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));
                if (saveDialog.getSelection().equalsIgnoreCase("YES"))
                    exit = promptToSave();
                if (saveDialog.getSelection().equalsIgnoreCase("CANCEL"))
                    exit = false;
            }
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        handleSaveRequest();
        return true;
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
    }

    public boolean getGameOver() {
        return gameover;
    }
}
